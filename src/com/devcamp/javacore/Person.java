package com.devcamp.javacore;

public class Person {
    // thuộc tính
    public String name;
    public int age;
    double weight;
    long salary;
    String[] pets;

    public Person() {
        this.name = "ngo thi hoang nghia";
        this.age = 20;
        this.weight = 50;
        this.salary = 12000000;
        this.pets = new String[] { "Cat", "Dog", "Bird" };

    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        this.weight = 50;

    }

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, int age, double weight, long salary) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
    }

    public Person(String name, int age, double weight, long salary, String[] pets) {
        this(name, age);
        // this dai dien cho class
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }

    // phuong thuc thuong
    public void showInfo() {
        System.out.println("ten" + this.name);
        System.out.println("tuoi" + this.age);
        System.out.println("can nang" + this.weight);
        System.out.println("thu nhap" + this.salary);
        System.out.println("vat nuoi" + this.pets.toString());
        if (this.pets != null) {
            for (String pet : this.pets) {
                System.out.print(pet + " " + ",");
            }
        }

    }

    public String Info() {
        return this.name + "," + this.age;
    }

    @Override
    public String toString() {
        String result = "Ten: " + this.name;
        result += "Tuoi: " + this.age;
        result += "Can nang: " + this.weight;
        result += "Thu nhap: " + this.salary;
        result += "Vat nuoi: [";
        if (this.pets != null) {
            for (String pet : this.pets) {
                result += pet + ",";
            }
        }
        result += "]";
        return result;
    }

}
