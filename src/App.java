import java.util.ArrayList;
import com.devcamp.javacore.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Person person1 = new Person();
        // person1.showInfo();
        System.out.println(person1);

        Person person2 = new Person("Nguyen Thi Na", 11);
        System.out.println(person2);

        ArrayList<Person> listPerson = new ArrayList<>();
        listPerson.add(person1);
        // listPerson.add(person2);
        listPerson.add(new Person());
        listPerson.add(new Person("Nguyen Phuc Quy"));

        for (int i = 0; i < listPerson.size(); i++) {
            Person person = listPerson.get(i);
            System.out.println(person);
        }

    }
}
